using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Core.Managers
{
    public class UiManager : BaseManager
    {
        [Inject]
        public UiManager(GameLoader gameLoader) : base(gameLoader)
        {
        }

        protected override async Task OnLoad()
        {
            await base.OnLoad();
            Debug.Log("Hello world");
            await Task.Delay(TimeSpan.FromSeconds(5));
            Debug.Log("UI Manager Loaded");
        }
    }
}