using System.Threading.Tasks;

namespace Core.Managers
{
    public interface IManager
    {
        Task Load();
    }
}