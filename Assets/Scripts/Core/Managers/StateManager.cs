using System.Threading.Tasks;
using UnityEngine;

namespace Core.Managers
{
    public class StateManager : BaseManager
    {
        public StateManager(GameLoader gameLoader) : base(gameLoader)
        {
        }

        protected override Task OnLoad()
        {
            Debug.Log("State Manager Loaded");
            return base.OnLoad();
        }
    }
}