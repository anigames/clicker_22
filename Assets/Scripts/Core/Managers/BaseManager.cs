using System.Threading.Tasks;
using Zenject;

namespace Core.Managers
{
    public abstract class BaseManager : IManager
    {
        [Inject]
        protected BaseManager(GameLoader gameLoader)
        {
            gameLoader.Load(this);
        }

        Task IManager.Load()
        {
            return OnLoad();
        }

        protected virtual Task OnLoad()
        {
            return Task.CompletedTask;
        }
    }
}