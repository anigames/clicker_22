using Core.Managers;
using Zenject;

namespace Core
{
    public class MainSceneInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameLoader>().AsSingle().NonLazy();
            InstallManagers();
        }

        private void InstallManagers()
        {
            Container.Bind<UiManager>().AsSingle().NonLazy();
            Container.Bind<StateManager>().AsSingle().NonLazy();
        }
    }
}