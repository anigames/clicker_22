using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Managers;
using ModestTree;

namespace Core
{
    public class GameLoader
    {
        private readonly Queue<IManager> _loadingQueue = new();
        private Task _loading;

        public void Load(IManager manager)
        {
            _loadingQueue.Enqueue(manager);

            if (_loading is { IsCompleted: false })
            {
                return;
            }

            _loading = LoadManager();
        }

        private async Task LoadManager()
        {
            if (_loadingQueue.IsEmpty())
            {
                return;
            }

            var manager = _loadingQueue.Dequeue();
            await manager.Load();
            await LoadManager();
        }
    }
}